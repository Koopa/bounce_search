$(document).ready(function() {
	
  // initialize physics
  init();
  play();
	
  $('#search').keyup(function() {
    var url = 'http://192.168.0.107:5757/?q=' + $(this).val();
    if ($(this).val() == ''){
      resultsFall(10, []);
    }
    $.getJSON(url, function(result) {
      var content = '';
      var searchPosition = $("#search").offset();
      var posX = searchPosition.left;
      var posY = searchPosition.top + 130;
      resultsFall(3, result);
      if (result != "no matches") {
      	for (var i = 0; i < result.length; i++) {
          $('body').append("<div data-id='"+result[i][0][0]+"' class='item' style='left:" + posX + "; top:" + (posY + i * 44) + "'><span class='bar' style='width: "+(result[i][2] * 100)+"%'></span><span>"+result[i][0][1]+" ("+result[i][0][2]+")</span></div>");
        }
      }
    });
  });
  
  $("input").focus();
  
  $(document).on('click', '.item', function() {
  	createImage($(this).data('id'));
  	createVideo();
  });
  
});
