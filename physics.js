var canvas;
var delta = [ 0, 0 ];
var gravity = { x: 0, y: 1 };
var stage = [ window.screenX, window.screenY, window.innerWidth, window.innerHeight ];
var wallsSet = false;
var walls = [];
var wall_thickness = 200;
var bodies, elements, text;
var PI2 = Math.PI * 2;
var timeStep = 1 / 15;
var iterations = 1;

function init() {

    canvas = document.getElementById( 'canvas' );

	// init box2d

	worldAABB = new b2AABB();
	worldAABB.minVertex.Set( -200, -200 );
	worldAABB.maxVertex.Set( window.innerWidth + 200, window.innerHeight + 200 );

	world = new b2World( worldAABB, new b2Vec2( 0, 0 ), true );

	setWalls();
	reset();
}

function reset() {

	var i;

	if ( bodies ) {

		for ( i = 0; i < bodies.length; i++ ) {

			var body = bodies[ i ]
			canvas.removeChild( body.GetUserData().element );
			world.DestroyBody( body );
			body = null;
		}
	}

	// color theme
	//theme = themes[ Math.random() * themes.length >> 0 ];
	//document.body.style[ 'backgroundColor' ] = theme[ 0 ];

	bodies = [];
	elements = [];


}

function play() {
	setInterval( loop, 1000 / 40 );
}

function loop() {
	
	if (getBrowserDimensions()) {
		setWalls();
	}

	delta[0] += (0 - delta[0]) * .5;
	delta[1] += (0 - delta[1]) * .5;

	world.m_gravity.x = gravity.x * 350 + delta[0];
	world.m_gravity.y = gravity.y * 350 + delta[1];

	world.Step(timeStep, iterations);
	
	for (i = 0; i < bodies.length; i++) {
		
		var body = bodies[i];
		var element = elements[i];
		
		var rect = element.getBoundingClientRect();
		
		if (rect.right < 0
		 || rect.left > stage[2]
		 || rect.bottom < 0
		 || rect.top > stage[3]) {
		  element.remove();
		  world.DestroyBody(body);
		  body = null;
		  bodies.splice(i, 1);
		  elements.splice(i, 1);
		  return;
		}

		element.style.left = (body.m_position0.x - (element.width >> 1)) + 'px';
		element.style.top = (body.m_position0.y - (element.height >> 1)) + 'px';

		if (element.tagName == 'DIV') {

			var style = 'rotate(' + (body.m_rotation0 * 57.2957795) + 'deg) translateZ(0)';
			element.style.WebkitTransform = style;
			element.style.MozTransform = style;
			element.style.OTransform = style;
			element.style.msTransform = style;
			element.style.transform = style;

		}

	}

}

function resultsFall(limit, newResults) {
	
	// loop through items
	var nodes = document.getElementsByClassName('item');

	var i = 0;
	var count = 0;

	if (elements.length > 30) {
      limit = 0;
	}
	
    while (i < nodes.length) {
      var item = nodes[0];
      var rect = item.getBoundingClientRect();
      var width = item.clientWidth;
      if (count < limit) {
      	var found = false;
      	for (var j = 0; j < newResults.length; j++) {
      	  if (newResults[j][0][0] == item.getAttribute("data-id")) {
      	  	found = true;
      	  	break;
      	  }
      	}
      	if (!found) {
      	  createResult(rect.right - width / 2, rect.top, item, 1);
      	  count++;
      	}
      }
      item.remove();
    }

}

function createVideo() {
	
	//var link = 
	var element = $('<iframe width="420" height="315" src="https://www.youtube.com/embed/xAZg-7AfnjU?autoplay=1" frameborder="0" allowfullscreen></iframe>')[0];
	var width, height;
	
	x = stage[2] / 2;
	y = stage[3] / 2;
	
	width = 420;
    height = 315;

	
	element.width = width;
	element.height = height;
	element.style.position = 'absolute';
	element.style.left = (x - width / 2) + 'px';
	element.style.top = y + 'px';
	element.style.WebkitTransform = 'translateZ(0)';
	element.style.MozTransform = 'translateZ(0)';
	element.style.OTransform = 'translateZ(0)';
	element.style.msTransform = 'translateZ(0)';
	element.style.transform = 'translateZ(0)';
	
	//element.className = "image";
	
	canvas.appendChild(element);

	elements.push( element );

	var b2body = new b2BodyDef();

	var box = new b2BoxDef();
	box.density = 1;
	box.friction = 0.2;
	box.restitution = 1;
	box.extents.Set(width / 2, height / 2);
	b2body.AddShape(box);
	b2body.userData = {element: element};

	b2body.position.Set( x, y );
	b2body.linearVelocity.Set( Math.random() * 400 - 200, Math.random() * 400 - 200 );
	bodies.push( world.CreateBody(b2body) );
}

function createImage(id) {
	
	// var element = document.createElement("div");
	var element = $("<img src='https://usercontent.googleapis.com/freebase/v1/image/"+id.replace(".", "/")+"?maxwidth=200&maxheight=200&mode=fillcropmid&key=AIzaSyBPxco-czHGuJ22prix7R6JKGx6TX18VOY'/>")[0];
	var width, height;
	
	x = stage[2] / 2;
	y = stage[3] / 2;
	
	width = 200;
    height = 200;

	
	element.width = width;
	element.height = height;
	element.style.position = 'absolute';
	element.style.left = (x - width / 2) + 'px';
	element.style.top = y + 'px';
	element.style.WebkitTransform = 'translateZ(0)';
	element.style.MozTransform = 'translateZ(0)';
	element.style.OTransform = 'translateZ(0)';
	element.style.msTransform = 'translateZ(0)';
	element.style.transform = 'translateZ(0)';
	
	element.className = "image";
	
	canvas.appendChild(element);

	elements.push( element );

	var b2body = new b2BodyDef();

	var box = new b2BoxDef();
	box.density = 1;
	box.friction = 0.2;
	box.restitution = 1;
	box.extents.Set(width / 2, height / 2);
	b2body.AddShape(box);
	b2body.userData = {element: element};

	b2body.position.Set( x, y );
	b2body.linearVelocity.Set( Math.random() * 400 - 200, Math.random() * 400 - 200 );
	bodies.push( world.CreateBody(b2body) );
}


function createResult(x, y, el, restitution) {
	
    // var element = document.createElement("div");
	var element = el.cloneNode(true)
	var width, height;
	
	width = el.clientWidth;
    height = el.clientHeight;

	
	element.width = width;
	element.height = height;
	element.style.position = 'absolute';
	element.style.left = (x - width / 2) + 'px';
	element.style.top = y + 'px';
	element.style.WebkitTransform = 'translateZ(0)';
	element.style.MozTransform = 'translateZ(0)';
	element.style.OTransform = 'translateZ(0)';
	element.style.msTransform = 'translateZ(0)';
	element.style.transform = 'translateZ(0)';
	
	element.className = "item2";
	
	canvas.appendChild(element);

	elements.push( element );

	var b2body = new b2BodyDef();

	var box = new b2BoxDef();
	box.density = 1;
	box.friction = 0.2;
	box.restitution = restitution;
	box.extents.Set(width / 2, height / 2);
	b2body.AddShape(box);
	b2body.userData = {element: element};

	b2body.position.Set( x, y );
	b2body.linearVelocity.Set( Math.random() * 400 - 200, Math.random() * 400 - 200 );
	bodies.push( world.CreateBody(b2body) );
}




function setWalls() {

	if (wallsSet) {

		world.DestroyBody(walls[0]);

		walls[0] = null; 
	}

	walls[0] = createBox(world, stage[2] / 2, stage[3] + wall_thickness, stage[2], wall_thickness); 

	wallsSetted = true;

}

function createBox(world, x, y, width, height, fixed) {

	if (typeof(fixed) == 'undefined') {

		fixed = true;

	}

	var boxSd = new b2BoxDef();

	if (!fixed) {

		boxSd.density = 1.0;

	}

	boxSd.extents.Set(width, height);

	var boxBd = new b2BodyDef();
	boxBd.AddShape(boxSd);
	boxBd.position.Set(x,y);

	return world.CreateBody(boxBd);

}

function getBrowserDimensions() {

	var changed = false;

	if (stage[0] != window.screenX) {

		delta[0] = (window.screenX - stage[0]) * 50;
		stage[0] = window.screenX;
		changed = true;

	}

	if (stage[1] != window.screenY) {

		delta[1] = (window.screenY - stage[1]) * 50;
		stage[1] = window.screenY;
		changed = true;

	}

	if (stage[2] != window.innerWidth) {

		stage[2] = window.innerWidth;
		changed = true;

	}

	if (stage[3] != window.innerHeight) {

		stage[3] = window.innerHeight;
		changed = true;

	}

	return changed;

}
