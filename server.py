"""
Copyright 2015, University of Freiburg.
Chair of Algorithms and Data Structures.
Frank Gelhausen <frank.gelhausen@gmail.com>
"""

import socket
import time
import sys
import math
import re
import json

class QgramIndex:
    """ A simple q-gram index, as explained in the lecture. """

    def __init__(self, q):
        """ Create an empty inverted index. """

        self.inverted_lists = dict()
        self.records = list()
        self.computations = 0
        self.q = q

    def read_from_file(self, file_name):
        with open(file_name) as file:
            record_id = 0
            for record in file:
                record_id += 1
                split_record = re.split(r'\t+', record)
                self.records.append(split_record)
                for qgram in self.qgrams(split_record[1]):
                    if len(qgram) > 0:
                        """ If qgram seen for first time, create empty inverted
                        list for it. """
                        if qgram not in self.inverted_lists:
                            self.inverted_lists[qgram] = list()
                        self.inverted_lists[qgram].append(record_id)

    def qgrams(self, record):
        record = re.sub("\W+", "", record).lower()
        qgrams = []
        pad = "$" * (self.q - 1)
        record = pad + record
        for i in range(0, len(record) - self.q + 1):
            qgrams.append(record[i:i + self.q])
        return qgrams

    @staticmethod
    def merge(lists):
        res = dict()
        for i in range(0, len(lists)):
            for j in range(0, len(lists[i])):
                if lists[i][j] in res:
                    res[lists[i][j]] += 1
                else:
                    res[lists[i][j]] = 1
        return res

    @staticmethod
    def compute_ped(s, p, delta):
        s = re.sub("\W+", "", s).lower()
        p = re.sub("\W+", "", p).lower()

        if len(s) < len(p):
            limit = min(len(p) + 1, len(s) + 1 + delta)
        else:
            limit = len(p) + 1
        grid = [[0 for x in range(int(limit))] for x in range(len(s) + 1)]
        for i in range(len(s) + 1):
            grid[i][0] = i
        for i in range(int(limit)):
            grid[0][i] = i

        for i in range(1, len(s) + 1):
            for j in range(1, int(limit)):
                if (s[i-1] != p[j-1]):
                    mini = min(grid[i-1][j-1] + 1,
                               grid[i][j-1] + 1,
                               grid[i-1][j] + 1)
                else:
                    mini = grid[i-1][j-1]
                grid[i][j] = mini

        return min(grid[len(s)])

    def find_matches(self, prefix, delta, k=5, use_qindex=True):
        self.computations = 0
        results = list()
        if use_qindex is True:
            l = list()
            for qgram in self.qgrams(prefix):
                if qgram in self.inverted_lists:
                    l.append(self.inverted_lists[qgram])
            l = self.merge(l)
            l = sorted(l.items(), key=lambda x: x[1], reverse=True)
            for item in l:
                if item[1] < len(prefix) - self.q * delta:
                    continue
                difference = self.compute_ped(prefix,
                                              self.records[item[0]-1][1], delta)
                self.computations += 1
                if difference <= delta:
                    if delta == 0:
                        error = 1
                    else:
                        error = 1 - (difference / delta)
                    param = 0.6
                    value = math.log(float(self.records[item[0]-1][3])) / math.log(1501781)
                    value = param * value + (1-param) * error
                    results.append((self.records[item[0]-1], difference, "{:.2f}".format(value) ))
        else:
            for record in self.records:
                difference = self.compute_ped(prefix, record[1], delta)
                self.computations += 1
                if difference <= delta:
                    results.append((record, difference))
        results.sort(key=lambda x: x[2], reverse=True)
        return results[:k]


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python3 server.py <file> <port>")
        exit(1)
    input_file = sys.argv[1]
    port = int(sys.argv[2])
    qi = QgramIndex(3)
    qi.read_from_file(input_file)
    # Create communication socket and listen on given port.
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(('', port))
    server.listen(5)
    print(socket.gethostname())
    # Server loop.
    while True:
        print("\x1b[1mWaiting for requests on port %d ... \x1b[0m" % port)
        (client, address) = server.accept()
        print("Incoming request at " + time.ctime())
        message = client.recv(8192).decode("ascii")
        match = re.match("^GET /(.*) HTTP", message)
        content_type = "text/plain"
        content = ""
        if (match):
            query = match.group(1)
            if query == "":
                query = "search.html"
            # check for files to open
            is_path = re.match("/", query)
            if not is_path:
                try:
                    with open(query) as file:
                        content = file.read()
                        if query.endswith(".html"):
                            content_type = "text/html"
                        elif query.endswith(".css"):
                            content_type = "text/css"
                        elif query.endswith(".js"):
                            content_type = "application/javascript"
                except:
                    if query[0:3] == "?q=":
                        search = query[3:].replace("%20", " ")
                        search = re.sub("\W+", "", search).lower()
                        delta = math.floor(len(search) / 4)
                        result = qi.find_matches(search, delta, k=10)
                        if len(result) == 0:
                            content = json.dumps("no matches")
                        else:
                            content = json.dumps(result)
                    else:
                        content = ""
            else:
                content = ""
        # Send answer
        if len(content) > 0:
            content_length = len(content)
            answer = "HTTP/1.1 200 OK\r\n" \
                     "Access-Control-Allow-Origin: *\r\n" \
                     "Content-Length: %d\r\n" \
                     "Content-Type: %s\r\n" \
                     "\r\n%s" % (content_length, content_type, content)
        else:
            with open("./404.html") as file:
                content = file.read()
                answer = "HTTP/1.1 404 Not Found\r\n" \
                         "\r\n%s" % (content)
        client.send(answer.encode("ascii"))
        client.close()
